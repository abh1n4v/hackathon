var EventSource = require('eventsource'),
	mysql	    = require('mysql'),
	moment		= require('moment'),
	tz		    = require('moment-timezone'),
	http        = require('http'),
	express		= require('express'),
	bodyParser  = require('body-parser'),
	PubSub		= require('pubsub-js'),
	settings	= require('./settings');

app = express();
app.use(express.static(__dirname + '/'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());;

var connection = mysql.createConnection({
  host     : settings.DATABASE.HOST,
  user     : settings.DATABASE.USER,
  password : settings.DATABASE.PASSWORD,
  database : settings.DATABASE.DATABASE
});
connection.connect();


/* 
	EVENT SOURCE DEF.
*/
var source = new EventSource(settings.KONNECT.API_URL + settings.KONNECT.API_KEY + '/konnect');
source.addEventListener('message', eventSubscriber);
source.addEventListener('error', function(error) {
    console.log('There was some problem with the eventsource.')
    console.log(error);
});
source.addEventListener('open', function() {
    console.log('Hooked up with event source.');
});


/*
	SUBSCRIBERS
*/

function eventSubscriber(event) {
	console.log(event);
	event_data = JSON.parse(event.data);

	if (event_data.type == "HANGUP") {
		console.log('Got a HANGUP event = ' + event_data);
		createAndSendSms(event_data.caller, event_data.k_number, event_data, function(has_changes) {
			if (has_changes) {
				PubSub.publish('new_cdr', 'New CDR Inserted!')
			}
		});
	}
}


/* FCUNTIONS */
function createAndSendSms(caller, sr_number, event_data, callback) {
			checkIfValidHotlineNumber(sr_number, function(team_name) {
				if(team_name) {
						text = `Thanks for voting team ${team_name}.`;
						
						checkIfAlreadyVoted(caller, sr_number, function(duplicate){
							if (!duplicate) {
								writeCdr(event_data, callback);	
								sendSms(caller, text);
							}
							else {
								text = `You have already voted for team ${team_name}`;
								sendSms(caller, text);
							}
							
						});
						
					}
				else {
					text = 'The hotline number has not been assigned to any team.'
					sendSms(caller, text);
				}
			});			
} 

function checkIfValidHotlineNumber(sr_number, callback) {
	query = `
		SELECT group_name FROM team WHERE hotline_number = "${sr_number}"
	`
	connection.query(query, function(err, result) {
		if (result[0].group_name) {
			group_name = result[0].group_name
			callback(group_name)
		}
		else {
			callback(false)
		}
	})
}


function checkIfValidCaller(caller, callback) {
	query = `
		SELECT count(*) as count FROM participants WHERE phone_number = "${caller}"
	`
	connection.query(query, function(err, result) {
		if (result[0].count) {
			callback(true)
		}
		else {
			callback(false)
		}
	})
}


function checkIfAlreadyVoted(caller, sr_number, callback) {
	query = `
		SELECT count(*) as count FROM cdr WHERE caller = "${caller}" and hotline_number = "${sr_number}"
	`
	connection.query(query, function(err, result) {
		console.log('VOTE query result' + result)
		if (result[0].count) {
			callback(true)
		}
		else {
			callback(false)
		}
	});
}

function writeCdr(event_data, callback) {
	start_time = moment().tz('Asia/Kolkata').format('YYYY-MM-DD HH:mm:ss')

	query = `INSERT INTO cdr(callid, caller, hotline_number, time) VALUES ("${event_data.uuid}", "${event_data.caller}", "${event_data.k_number}", "${start_time}")`
	console.log('Query  ' + query);

	connection.query(query, function(err, result) {
		if (!err) {
			console.log('INSERTED CDR with UUID = '+ event_data.uuid)
			callback(true)
		}
	})
	
}

function sendSms(number, text) {
	text = text+"\nPowered By Knowlarity http://goo.gl/gWPP6V"
	url = settings.SMS.API_URL + `?mobile_number=${number}&message=${text}&sms_type=t`

	console.log("Sending SMS using URL = " + url);
	http.get(url, function(response) {
			console.log('SMS API result = ' + response);
	});
}

function retreiveResults(callback) {
	leaderboard_query = `
		SELECT team.group_name as team, cdr.hotline_number as hotline_number, count(*) as votes 
			FROM team INNER JOIN cdr ON team.hotline_number = cdr.hotline_number
				WHERE cdr.time >= "${settings.HACKATHON.start_time}" AND cdr.time <= "${settings.HACKATHON.end_time}"
				GROUP BY cdr.hotline_number, team.group_name
				ORDER BY votes desc LIMIT 10; 
	`

	participant_count_query = `
		SELECT count(*) as participant_count
		   FROM team;
	`

	participation_count = `
		SELECT count(*) as participation_count
			FROM cdr 
				WHERE cdr.time >= "${settings.HACKATHON.start_time}" AND cdr.time <= "${settings.HACKATHON.end_time}"
	`

	participation_count_last_hour = `
		SELECT count(*) as participation_count
			FROM cdr WHERE cdr.time >= DATE_SUB(NOW(),INTERVAL 1 HOUR); 
	`

	connection.query(leaderboard_query, function(leaderboard_error, leaderboard_result) {
		connection.query(participant_count_query, function(participant_error, participant_result) {
			connection.query(participation_count, function(participantion_error, participation_result){
				connection.query(participation_count_last_hour, function(participantion_lasthr_error, participation_lasthr_result){
					console.log('ERRORS' + leaderboard_error + participantion_error, participant_error + participantion_lasthr_error);
					result = {
						leaderboard: leaderboard_result,
						participant_count: participant_result[0].participant_count,
						participation_count: participation_result[0].participation_count,
						participation_count_last_hour: participation_lasthr_result[0].participation_count,
						last_updated: moment().tz('Asia/Kolkata').format('DD MMMM hh:mm a'),
						poll_start_time: moment(settings.HACKATHON.start_time).format('DD MMMM hh:mm a'),
						poll_end_time: moment(settings.HACKATHON.end_time).format('DD MMMM hh:mm a')
					}
					callback(JSON.stringify(result));
				});
			});
		});
	});

}

/* 
	API
*/
app.get('/leaderboard-stream', function(request, response) {

	response.header("Access-Control-Allow-Origin", "*");
	response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	response.setTimeout(0);   


	response.writeHead(200, {
	  'Connection': 'keep-alive',
	  'Content-Type': 'text/event-stream',
	  'Cache-Control': 'no-cache',
	});

	retreiveResults(function(result){
		response.write("id: " + Date.now() + "\ndata: " + result + "\n\n");
		response.flush();

		PubSub.subscribe('new_cdr', function() {
			retreiveResults(function(live_results) {
				response.write("id: " + Date.now() + "\ndata: " + live_results + "\n\n");
				response.flush();
			});
		});
	});
})


var port = 8001;
app.listen(port, function() {
  console.log("Running at Port " + port);
});
